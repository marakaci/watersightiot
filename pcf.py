import smbus
import time

bus = smbus.SMBus(1)
address = 0x48


def bearing255():
    bear = bus.read_byte_data(address, 1)
    return bear


while True:  # this returns the value to 1 decimal place in degrees.
    with open("tur", "w") as f:
        bear255 = str(bearing255())
        print(bear255)
        f.write(bear255)  # this returns the value as a byte between 0 and 255.
    time.sleep(1)
