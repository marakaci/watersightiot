import json

import requests

from data_class import SensorData
from json_encoders import DataClassJSONEncoder


class ServerConnector:
    local_server_url = "http://192.168.0.104:8000"
    production_url = "https://marakaci.pythonanywhere.com"
    port = 8000

    device_username = 'public_device'
    device_password = 'public_device'
    device_pk = 2
    api_url_sensors = f"api/device/{device_pk}/indicators/"
    api_url_login = f"api/login/"

    def __init__(self, is_production=False, auth_token=None):
        self.server_ip = self.production_url if is_production else self.local_server_url
        self.auth_token = auth_token
        self.headers = {
            "Content-type": "application/json"
        }

    def build_full_url(self, api_url=api_url_sensors):
        return f"{self.server_ip}/{api_url}"

    def authenticate_device(self):
        url = self.build_full_url(api_url=self.api_url_login)
        payload = {
            "username": self.device_username,
            "password": self.device_password
        }
        print("authentication")
        response = json.loads(requests.post(url, data=json.dumps(payload), headers=self.headers).content)
        self.auth_token = response.get("token")
        self.headers['Authorization'] = f"JWT {self.auth_token}"
        print("authenticated")

    def send_sensors_data(self, data: SensorData):
        if self.auth_token is None:
            self.authenticate_device()

        url = self.build_full_url()
        response = json.loads(requests.post(url, data=json.dumps(data, cls=DataClassJSONEncoder),
                                            headers=self.headers).content)
        print("results sent")
        print(response)


if __name__ == '__main__':
    sc = ServerConnector()
    sc.send_sensors_data(SensorData())
