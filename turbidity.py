import smbus2 as smbus

from decorators import logging


class TurbiditySensor:
    ADDRESS = 0x48

    def __init__(self):
        self.last_correct_turbidity: float = None
        self.bus = smbus.SMBus(1)

    def __is_conspicuous_new_turbidity(self, turbidity: float) -> bool:
        return abs(self.last_correct_turbidity - turbidity) > 50 or turbidity == 0 or turbidity > 100

    def __should_overwrite_last_correct_turbidity(self, temperature: float) -> bool:
        return not self.last_correct_turbidity or not self.__is_conspicuous_new_turbidity(temperature)

    def _extract_turbidity(self):
        try:
            turbidity = float(self.bus.read_byte_data(self.ADDRESS, 1))
            print('raw turbidity %s' % turbidity)
            turbidity = 100 - (turbidity / 255 * 100)
        except ValueError:
            turbidity = self.last_correct_turbidity
        if turbidity < 0:
            turbidity = 0
        return turbidity

    @logging(name='turbidity')
    def get_turbidity(self) -> float:
        turbidity = self._extract_turbidity()
        if self.__should_overwrite_last_correct_turbidity(turbidity):
            self.last_correct_turbidity = turbidity
        return self.last_correct_turbidity


if __name__ == '__main__':
    print(TurbiditySensor().get_turbidity())
