import os

from decorators import logging
from exceptions import IncorrectWiring


class TemperatureSensor:
    SENSORS_DIR = "/sys/bus/w1/devices"

    def __init__(self):
        self.last_correct_temperature: float = None

    def get_temperature_sensor_file(self) -> str:
        for dr in os.listdir(self.SENSORS_DIR):
            if dr.startswith("28"):
                return f"{self.SENSORS_DIR}/{dr}/w1_slave"
        raise IncorrectWiring("Temperature sensor is wired wrong")

    def __is_conspicuous_new_temperature(self, temperature: float) -> bool:
        return abs(self.last_correct_temperature - temperature) > 20 or temperature == 0 or temperature > 50

    def __should_overwrite_last_correct_temperature(self, temperature: float) -> bool:
        return not self.last_correct_temperature or not self.__is_conspicuous_new_temperature(temperature)

    @staticmethod
    def __extract_data_from_file(file):
        lines = file.readlines()
        assert len(lines), 2
        temperature_line = lines[1].strip("\n")
        temperature_line_chunks = temperature_line.split(" ")
        assert len(temperature_line_chunks), 10
        temperature_chunk = temperature_line_chunks[9]
        return temperature_chunk[2:]

    @logging(name='temperature')
    def get_temperature(self) -> float:
        sensor_file_path = self.get_temperature_sensor_file()
        with open(sensor_file_path) as f:
            temperature = float(self.__extract_data_from_file(f))
            celsius = temperature / 1000
            if self.__should_overwrite_last_correct_temperature(celsius):
                self.last_correct_temperature = celsius
            return self.last_correct_temperature
