from random import randint
from time import sleep

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.options import Options

from decorators import logging

from pyvirtualdisplay import Display


class Location:

    def __init__(self):
        display = Display(visible=0, size=(1, 1))
        display.start()

        chrome_options = Options()

        self.browser = webdriver.Chrome("./chromedriver", chrome_options=chrome_options)
        self.browser.get('https://google.com/maps/')
        self.lat = None
        self.lng = None

        handles = self.browser.window_handles
        self.browser.switch_to.window(handles[-1])

    def __lat_lng(self) -> (float, float):
        if not (self.lat and self.lng):
            lat_lng = self.browser.current_url.split("@")[1].split(',')[:2]
            self.lat, self.lng = float(lat_lng[0]), float(lat_lng[1])
        return self.lat, self.lng

    @logging(name='location')
    def get_lat_lng(self) -> (float, float):
        while True:
            try:
                print("start get coords")
                lat, lng = self.__lat_lng()
                print("found")
                self.browser.quit()
                return lat, lng
            except (NoSuchElementException, ValueError, IndexError) as e:
                print("not find")
                print("sleep")
                sleep(0.5)
                print("awake")

                continue


if __name__ == '__main__':
    Location().get_lat_lng()
