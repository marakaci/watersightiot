import json
import sys
from time import sleep

from data_class import SensorData
from gps import GPSSensor
from server_connector import ServerConnector
from temperature import TemperatureSensor
from turbidity import TurbiditySensor


class SensorsMaster:

    def __init__(self):
        self.is_production = False
        self.send_to_console = False
        self.get_command_line_params()

    def get_command_line_params(self):
        if len(sys.argv) > 1:
            self.is_production = json.loads(sys.argv[1])
            print("is production %s" % self.is_production)
        if len(sys.argv) > 2:
            self.send_to_console = json.loads(sys.argv[2])
            print("send to console %s" % self.is_production)

    def run(self):
        server_connector = ServerConnector(is_production=self.is_production)
        temperature_sensor = TemperatureSensor()
        gps_sensor = GPSSensor()
        turbidity_sensor = TurbiditySensor()
        previous_data: SensorData = SensorData()
        while True:
            temperature = temperature_sensor.get_temperature()
            lat, lng = gps_sensor.get_lat_lng()
            turbidity = turbidity_sensor.get_turbidity()
            data = SensorData(temperature=temperature, lat=lat, lng=lng, turbidity=turbidity)
            print("data is ready")

            if data.has_changed(previous_data):
                data.notify_government_if_needed(previous_data)
                server_connector.send_sensors_data(data)
                previous_data = data
            else:
                print("data hasn't changed a lot")
            print(data)
            print("next circle")
            sleep(1)


if __name__ == '__main__':
    sensors_master = SensorsMaster()
    sensors_master.run()
