from __future__ import annotations
from dataclasses import dataclass

from send_email import Email


@dataclass
class SensorData:
    temperature: float = 0
    lat: float = 0
    lng: float = 0
    turbidity: float = 0

    def __has_temperature_changed(self, other: SensorData) -> bool:
        return abs(self.temperature - other.temperature) > 0.2

    def __has_lat_changed(self, other: SensorData) -> bool:
        return abs(self.lat - other.lat) > 0.01

    def __has_lng_changed(self, other: SensorData) -> bool:
        return abs(self.lng - other.lng) > 0.01

    def __has_turbidity_changed(self, other: SensorData) -> bool:
        return abs(self.turbidity - other.turbidity) > 5

    def __has_turbidity_changed_critically(self, other: SensorData) -> bool:
        return abs(self.turbidity - other.turbidity) > 15

    def has_changed(self, other: SensorData) -> bool:
        return self.__has_temperature_changed(other) or \
               self.__has_lat_changed(other) or \
               self.__has_lng_changed(other) or \
               self.__has_turbidity_changed(other)

    def notify_government_if_needed(self, other):
        if self.__has_turbidity_changed_critically(other):
            Email.send_email(
                email_text=f"There is a caution of pollution in area {str(self.lat)} {str(self.lng)}")
