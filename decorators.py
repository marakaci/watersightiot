import functools


def logging(name):
    def decorator_use_name(func):
        @functools.wraps(func)
        def wrapper_use_name(*args, **kwargs):
            value = func(*args, **kwargs)
            print("%s:%s" % (name, str(value)))
            return value

        return wrapper_use_name

    return decorator_use_name
